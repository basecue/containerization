from django.shortcuts import render


# Create your views here.
import example_app.models


def index(request):
    content = example_app.models.Content.objects.all().order_by('?')[0]
    return render(request=request, context={'content': content}, template_name='index.html')
