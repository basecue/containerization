FROM python:3.7.1

WORKDIR /src/

COPY . .

RUN pip install -r requirements.txt
RUN pip install uwsgi

VOLUME /data
ENTRYPOINT ["uwsgi", "--http-socket", ":9090", "--wsgi-file", "containerization/wsgi.py"]
